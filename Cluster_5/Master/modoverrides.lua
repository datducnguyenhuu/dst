return {
  ["workshop-1207269058"]={ configuration_options={  }, enabled=true },
  ["workshop-2078243581"]={
    configuration_options={ Blue=0, Display="target", Green=0, Projectile=true, Red=1, Type="hit" },
    enabled=true 
  },
  ["workshop-374550642"]={ configuration_options={ MAXSTACKSIZE=250 }, enabled=true },
  ["workshop-375850593"]={ configuration_options={  }, enabled=true },
  ["workshop-378160973"]={
    configuration_options={
      ENABLEPINGS=true,
      FIREOPTIONS=2,
      OVERRIDEMODE=false,
      SHAREMINIMAPPROGRESS=true,
      SHOWFIREICONS=true,
      SHOWPLAYERICONS=true,
      SHOWPLAYERSOPTIONS=2 
    },
    enabled=true 
  },
  ["workshop-661253977"]={
    configuration_options={ amudiao=true, baodiao=1, kong=0, rendiao=2, zbdiao=true },
    enabled=true 
  },
  ["workshop-666155465"]={
    configuration_options={
      chestB=-1,
      chestG=-1,
      chestR=-1,
      food_estimation=-1,
      food_order=0,
      food_style=0,
      lang="auto",
      show_food_units=-1,
      show_uses=-1 
    },
    enabled=true 
  } 
}